from rest_framework import serializers
from authApp.models.Comments import Comments

class CommentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comments
        fields = ['idComments', 'commentContent' , 'idUserFK' , 'idPostFK' , 'dateComment']
    def create(self, validated_data):
        CommentsInstance = Comments.objects.create(**validated_data)
        return CommentsInstance

    def to_representation(self, obj):
        comments = Comments.objects.get(idComments=obj.idComments)
        return {
            'idComments': comments.idComments,
            'commentContent': comments.commentContent,
            'idUserFK': comments.idUserFK_id,
            'idPostFK': comments.idPostFK_id,
            'dateComment': comments.dateComment,
        }