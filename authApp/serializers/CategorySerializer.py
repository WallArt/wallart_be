from rest_framework import serializers
from authApp.models.Category import Category

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['idCategory', 'nameCategory']
        
    def create(self, validated_data):
        CategoryInstance = Category.objects.create(**validated_data)
        return CategoryInstance

    def to_representation(self, obj):
        category = Category.objects.get(idCategory=obj.idCategory)
        return {
            'idCategory': category.idCategory,
            'nameCategory': category.nameCategory,
        }