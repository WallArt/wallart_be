from rest_framework import serializers
from authApp.models.Interest import Interest

class InterestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Interest
        fields = ['idInterest', 'idUserFK' , 'idPostFK' , 'interestDate']
    def create(self, validated_data):
        InterestInstance = Interest.objects.create(**validated_data)
        return InterestInstance

    def to_representation(self, obj):
        interest = Interest.objects.get(idInterest=obj.idInterest)
        return {
            'idInterest': interest.idInterest,
            'idUserFK': interest.idUserFK_id,
            'idPostFK': interest.idPostFK_id,
            'interestDate': interest.interestDate,
        }