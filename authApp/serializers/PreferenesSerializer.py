from rest_framework import serializers
from authApp.models.Preferences import Preferences

class PreferencesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Preferences
        fields = ['idPreferences', 'idUserFK' , 'idCategoryFK']
        
    def create(self, validated_data):
        PreferencesInstance = Preferences.objects.create(**validated_data)
        return PreferencesInstance

    def to_representation(self, obj):
        preferences = Preferences.objects.get(idPreferences=obj.idPreferences)
        return {
            'idPreferences': preferences.idCategory,
            'idUserFk': preferences.idUserFK_id,
            'idCategoryFK': preferences.idCategoryFK_id,
        }