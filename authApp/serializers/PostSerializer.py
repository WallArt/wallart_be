from rest_framework import serializers
from authApp.models.Post import Post

class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ['idPost', 'titlePost' , 'descriptionPost' , 'imagePost' , 'datePost' , 'idUserFK' , 'idCategoryFK']
    def create(self, validated_data):
        PostInstance = Post.objects.create(**validated_data)
        return PostInstance

    def to_representation(self, obj):
        post = Post.objects.get(idPost=obj.idPost)
        return {
            'idPost': post.idCategory,
            'titlePost': post.nameCategory,
            'descriptionPost': post.descriptionPost,
            'imagePost': post.imagePost,
            'datePost': post.datePost,
            'idUserFK': post.idUserFK_id,
            'idCategoryFK': post.idCategoryFK_id,

        }