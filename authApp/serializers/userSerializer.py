from rest_framework import serializers
from authApp.models.User import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'nameUser', 'lastNameUser', 'username',
         'password', 'emailUser', 'cellphoneUser' , 'dateBirthUser',
         'genderUser' , 'statusUser' , 'userType' , 'imageUser']
        
    def create(self, validated_data):
        userInstance = User.objects.create(**validated_data)
        return userInstance

    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        return {
            'id': user.id,
            'nameUser': user.nameUser,
            'lastNameUser': user.lastNameUser,
            'username': user.username,
            'password': user.password,
            'emailUser': user.emailUser,
            'cellphoneUser': user.cellphoneUser,
            'dateBirthUser': user.dateBirthUser,
            'genderUser': user.genderUser,
            'statusUser': user.statusUser,
            'userType': user.userType,
            'imageUser': user.imageUser,
        }