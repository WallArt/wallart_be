from .CategorySerializer import CategorySerializer
from .CommentsSerializer import CommentsSerializer
from .InterestSerializer import InterestSerializer
from .PostSerializer import PostSerializer
from .PreferenesSerializer import PreferencesSerializer
from .userSerializer import UserSerializer