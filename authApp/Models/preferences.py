from django.db import models
from .User import User
from .Category import Category

class Preferences(models.Model):
    idPreferences = models.AutoField(primary_key=True)
    idUserFK = models.ManyToManyField(User)
    idCategoryFK =  models.ManyToManyField(Category)
    