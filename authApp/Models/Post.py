from django.db import models
from .User import User
from .Category import Category

class Post(models.Model):
    idPost = models.AutoField(primary_key=True)
    titlePost = models.CharField(max_length=25)
    descriptionPost = models.CharField(max_length=100)
    imagePost = models.CharField(max_length=75)
    datePost = models.DateField()
    idUserFK = models.ForeignKey(User, related_name='Post_idUserFK', on_delete=models.CASCADE) 
    idCategoryFK = models.ForeignKey(Category, related_name='Post_idCategoryFK', on_delete=models.CASCADE) 