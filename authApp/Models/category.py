from django.db import models

class Category (models.Model):
    idCategory = models.AutoField(primary_key=True)
    nameCategory = models.CharField(max_length=25)
